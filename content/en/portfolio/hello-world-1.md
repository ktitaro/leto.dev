---
slug: hello-world-1
site: https://google.com/
cover: test.jpeg
title: Stepping up omnichannel
company: Ace & Tate
excerpt: Studio Job was founded in 1998. Their highly acclaimed art and design work combines both traditional and modern techniques, producing once-in-a-lifetime objects for both the art world and for mass production.
challenge: Design and develop mobile app from scratch.
timeline:
  start: 123123123
  finish: 124124124
awards:
  - Awwwards - Site of the day
  - Awwwards - Best e-commerce
ui:
  preview:
    fg: '#fff'
    bg: '#000'
  overview:
    block_l:
      fg: '#000'
      bg: '#fff'
    block_r:
      fg: '#000'
      bg: '#ECD018'
---

<div class="block">
  <h3 class="block__title">
    Going back to paradise
  </h3>
</div>

<div class="block">
  <div class="block__half" style="background-image: url(/images/test.jpeg)"></div>
  <div class="block__half" style="background-image: url(/images/test.jpeg)"></div>
</div>

<div class="block" style="background-image: url(/images/test.jpeg)"></div>

<div class="block">
  <h3 class="block__title">
    Going back to paradise
  </h3>
</div>
