const path = require('path')
const glob = require('glob')

const ROOT_DIR = path.resolve(__dirname)
const SOURCE_DIR = path.join(ROOT_DIR, 'src')
const CONTENT_DIR = path.join(ROOT_DIR, 'content')

function getDynamicPaths(urlFilepathMap) {
  return [].concat(
    ...Object.keys(urlFilepathMap).map(url => {
      const filepathGlob = urlFilepathMap[url]
      return glob
        .sync(filepathGlob, { cwd: CONTENT_DIR })
        .map(filepath => `${url}/${path.basename(filepath, '.md')}`)
    })
  )
}

const dynamicPaths = getDynamicPaths({})

module.exports = {
  mode: 'universal',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  srcDir: SOURCE_DIR,
  loading: {
    color: '#000'
  },
  modules: [
    '@nuxtjs/style-resources',
    'nuxt-i18n',
  ],
  plugins: [
    '~/plugins/animejs',
  ],
  css: [
    'bulma/css/bulma.min.css',
    '~/assets/styles/css/variables.css',
    '~/assets/styles/css/typography.css',
  ],
  styleResources: {
    scss: [
      '~/assets/styles/scss/common-styles.scss',
      '~/assets/styles/scss/responsiveness.scss'
    ],
  },
  i18n: {
    locales: ['en', 'ru'],
    strategy: 'prefix_and_default',
    defaultLocale: 'en',
    vueI18nLoader: true
  },
  generate: {
    dir: 'public',
    routes: dynamicPaths,
  },
  build: {
    extend(config) {
      config.module.rules.push({
        test: /\.md$/,
        loader: 'frontmatter-markdown-loader',
        include: CONTENT_DIR,
      })
      config.module.rules.push({
        type: 'javascript/auto',
        loader: ['@kazupon/vue-i18n-loader'],
        resourceQuery: /blockType=i18n/,
      })
      config.resolve.alias['@content'] = CONTENT_DIR;
    },
  },
}
